﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    [SerializeField] private AudioClip FX_bombolles_lava;
    [SerializeField] private AudioClip FX_bombolles_lava1;
    [SerializeField] private AudioClip FX_button_Selection;
    [SerializeField] private AudioClip FX_empenta;
    [SerializeField] private AudioClip FX_gameOver;
    [SerializeField] private AudioClip FX_mort;
    [SerializeField] private AudioClip FX_ma;
    [SerializeField] private AudioClip FX_points;

    [SerializeField] [Range(0, 1)] private float volumLava;

    [SerializeField]private AudioSource efxSource;

    [SerializeField] private AudioSource efxSource2;

    public static SoundManager instance = null;

    // Use this for initialization
    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;//Avoid doing anything else
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void  PlaySound(string _sound)
    {
        switch (_sound)
        {
            case "FX_bombolles_lava":
                efxSource2.PlayOneShot(FX_bombolles_lava);
                break;
            case "FX_bombolles_lava1":
                efxSource2.PlayOneShot(FX_bombolles_lava1);
                break;
            case "FX_points":
                efxSource.PlayOneShot(FX_points);
                break;
            case "FX_ma":
                efxSource.PlayOneShot(FX_ma);
                break;
            case "FX_mort":
                efxSource.PlayOneShot(FX_mort);
                break;
            case "FX_gameOver":
                efxSource.PlayOneShot(FX_gameOver);
                break;
            case "FX_empenta":
                efxSource.PlayOneShot(FX_empenta);
                break;
            case "FX_button_Selection":
                efxSource.PlayOneShot(FX_button_Selection);
                break;
            default:
                Debug.Log("ERROR: el so no existeix, comprova el nom");
                break;
        }
    }

}
