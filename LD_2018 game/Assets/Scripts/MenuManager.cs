﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {


    [SerializeField] private GameObject[] panels;

    private GameObject actual;
	// Use this for initialization
	void Start ()
    {
        for (int i = 1; i < panels.Length; i++)
        {
            panels[i].SetActive(false);
        }


	}
	

    public void ActivateMenu(int _menu)
    {
        SoundManager.instance.PlaySound("FX_button_Selection");
        if (_menu == 0)
        {
            SceneManager.LoadScene("Prototype");
        }
        panels[_menu].gameObject.SetActive(true);
        actual = panels[_menu];
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void BackButton()
    {
        SoundManager.instance.PlaySound("FX_button_Selection");
        actual.SetActive(false);
    }
    public void ToMenu()
    {
        SceneManager.LoadScene("Menu");
        SoundManager.instance.PlaySound("FX_button_Selection");
    }
}
