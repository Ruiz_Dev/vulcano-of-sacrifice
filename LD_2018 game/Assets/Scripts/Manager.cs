﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Manager : MonoBehaviour
{

    public bool isOverAPerson = false;

    public float minCitizien;
    public int initCitizens;
    public List<GameObject> listOfCharacters = new List<GameObject>();


    public Sprite backGround;
    public float radius; float centreX, centerY;
    private Vector3 randomSpawnPos, center;


    public GameObject character;

    public float timeToSick;
    private float timetosickInit;
    public int initSicks;
    public int numberOfSicks;


    public float gameTime = 3;
    private bool inGame = false;

    //GUI

    public GameObject panelGameOver;
    public GameObject panelWin;
    public GameObject panelTime;
    public GameObject panelCharacters;

    private Text timeText;
    private Text charactersText;


    private void Awake()
    {
        backGround = GameObject.FindGameObjectWithTag("Background").GetComponent<SpriteRenderer>().sprite;
        panelGameOver.SetActive(false);
        panelWin.SetActive(false);

    }

    void Start()
    {
        timeText = panelTime.GetComponentInChildren<Text>();
        charactersText = panelCharacters.GetComponentInChildren<Text>();
        float centerX = backGround.bounds.size.x;
        float centerY = backGround.bounds.size.y;
        Vector3 centre = new Vector3(centerX, centerY, 0);

        for (int i = 0; i < initCitizens; i++)
        {
            SpawnPos();
            character = Instantiate(character, randomSpawnPos, Quaternion.identity);
            listOfCharacters.Add(character);
        }

        numberOfSicks = initSicks;
        foreach (GameObject c in listOfCharacters)
        {
            if (initSicks > 0)
            {
                c.GetComponent<finalCharacter>().currentState = finalCharacter.State.MAL;
                initSicks--;
            }
        }


        timetosickInit = timeToSick;
        inGame = true;
    }

    void Update()
    {
        timeText.text = "Time: " + (int)gameTime;
        charactersText.text = listOfCharacters.Count + " / " + minCitizien;
        if (inGame == true && gameTime <= 0 && minCitizien <= listOfCharacters.Count)
        {
            Win();
        }

        if (inGame == true && (minCitizien > listOfCharacters.Count || gameTime <= 0))
        {
            GameOver();
        }

        if (inGame == true)
        {
            gameTime -= Time.deltaTime;
            timeToSick -= Time.deltaTime;
            if (timeToSick < 0)
            {
                SpawnSicks();
                timeToSick = timetosickInit;
            }
            

        }


    }

    Vector3 SpawnPos()
    {

        randomSpawnPos = new Vector3(Random.Range(-9.5f, 10f), Random.Range(-5f, 5.5f), 0);
        float finalP = (randomSpawnPos - center).sqrMagnitude;
        if (finalP <= radius + 5)
        {
            SpawnPos();

        }

        return randomSpawnPos;
    }

    void SpawnSicks()
    {
        int numberOfSicksToAdd = Mathf.RoundToInt(numberOfSicks * 0.5f);
        if (numberOfSicks + numberOfSicksToAdd <= listOfCharacters.Count)
        {
            int idx = 0;
             while(numberOfSicksToAdd > 0 && idx <= listOfCharacters.Count)
                {
                    if (listOfCharacters[idx].GetComponent<finalCharacter>().currentState == finalCharacter.State.SA)
                    {
                        listOfCharacters[idx].GetComponent<finalCharacter>().currentState = finalCharacter.State.MAL;
                    numberOfSicksToAdd--;
                    }

                idx++;
               }
        }
        else
        {
            foreach (GameObject c in listOfCharacters)
            {
                c.GetComponent<finalCharacter>().currentState = finalCharacter.State.MAL;
            }
        }
    }


    void Win()
    {
        inGame = false;
        Debug.Log("win");
        for (int i = 0; i < listOfCharacters.Count; i++)
        {

            listOfCharacters[i].GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
            listOfCharacters[i].GetComponent<finalCharacter>().enabled = false;
        }

        panelWin.SetActive(true);

    }

    void GameOver()
    {
        Debug.Log("GameOVer");
        inGame = false;
        for (int i = 0; i < listOfCharacters.Count; i++)
        {

            listOfCharacters[i].GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
            listOfCharacters[i].GetComponent<finalCharacter>().enabled = false;
        }

        panelGameOver.SetActive(true);

    }

    public GameObject getActiveCharacter()
    {
        foreach(GameObject c in listOfCharacters)
        {
            if(c.GetComponent<finalCharacter>().isActive) { return c; }
        }

        return null;
    }
}
