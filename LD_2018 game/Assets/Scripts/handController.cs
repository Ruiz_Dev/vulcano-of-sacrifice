﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handController : MonoBehaviour {


    public GameObject manager;
    private GameObject character;
    public Animator animator;
    

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (manager.GetComponent<Manager>().isOverAPerson){
            //recorre la llista i troba el character
            character = manager.GetComponent<Manager>().getActiveCharacter();
        }
        if(character != null)
        {
            //character is active
            if (character.GetComponent<finalCharacter>().isActive)
            {
                if (!character.GetComponent<finalCharacter>().isBullet)
                {
                    if (Input.GetMouseButton(0))
                    {
                        Vector3 dragV = character.GetComponent<finalCharacter>().dragVector;
                        animator.SetBool("clickDown", true);
                        float angleHandZ = Mathf.Atan2(dragV.y, dragV.x) * Mathf.Rad2Deg + 90;
                        animator.transform.eulerAngles = new Vector3(0, 0, angleHandZ);
                        float handMargin = 1;
                        animator.transform.position = character.transform.position + dragV.normalized * handMargin;
                    }

                    
                }

            }
            if (Input.GetMouseButtonUp(0))
            {
                animator.SetBool("clickDown", false);
                SoundManager.instance.PlaySound("FX_ma");
            }
        }

    }
}
