﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    [SerializeField]private GameObject[] sprites;
    [SerializeField]private float incrementTimer;
    [SerializeField] private float incrementTimerElection;
    [SerializeField] private float activationExplosion;
    [SerializeField] private bool isActive;

    [SerializeField] private float T0;
    [SerializeField] private float T1;
    [SerializeField] private float T2;
    [SerializeField] private float T3;
    [SerializeField] private float finishAnimation;


    // Use this for initialization
    void Start ()
    {
        isActive = false;
        incrementTimer =0;
        activationExplosion = Random.Range(0,5);
        for (int i = 0; i < sprites.Length; i++)
        {
            sprites[i].SetActive(false);
        }
       
    }
	
	// Update is called once per frame
	void Update ()
    {

        incrementTimer += Time.deltaTime;
        incrementTimerElection += Time.deltaTime;

        if (isActive)
        {
            ExecuteExplosion();
        }
        else if (incrementTimerElection > activationExplosion)
        {
            incrementTimerElection = 0;
            int tmp = Random.Range(1,3);
            if (tmp >= 1 && tmp < 2)
            {
                isActive = true;
                incrementTimer = 0;
            }
        }

    }

    private void ExecuteExplosion()
    {
        if (incrementTimer < T0)
        {
            sprites[0].SetActive(true);
        }
        else if (incrementTimer < T1)
        {
            sprites[0].SetActive(false);
            sprites[1].SetActive(true);
        }
        else if (incrementTimer < T2)
        {
            sprites[1].SetActive(false);
            sprites[2].SetActive(true);
        }
        else if (incrementTimer < T3)
        {
            sprites[2].SetActive(false);
            sprites[3].SetActive(true);
        }
        else if (incrementTimer < finishAnimation)
        {
            sprites[3].SetActive(false);
            incrementTimer = 0;
            isActive = false;
            SoundManager.instance.PlaySound("FX_bombolles_lava");
            SoundManager.instance.PlaySound("FX_bombolles_lava1");
        }

    }


}
